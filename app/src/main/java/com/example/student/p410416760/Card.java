package com.example.student.p410416760;

/**
 * Created by student on 2017/11/9.
 */

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by toreal on 2017/10/12.
 */

public class Card extends FrameLayout {



    public Card(@NonNull Context context , int w) {
        super(context);

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(w-10 ,w-10);
        lp.setMargins(5,5,5,5);

        View view = new View(context);
        //view.setBackgroundColor(0x33ff00ff);
        addView(view,lp);

        tv= new TextView(context);
        // tv.setText(Integer.toString(0));
        tv.setTextSize(48);
        tv.setGravity(Gravity.CENTER);
        addView(tv,lp);



    }
    TextView tv;
    String value;

    public String getNum()
    {
        return value;
    }

    public void setNum( String num )
    {
        value = num;

        //if (num.equals("A"))
           //tv.setText("A");
        //else
           // tv.setText("");


        switch(num)
        {
            case "A":
                tv.setText("A");
                tv.setBackgroundColor(0xffeee4da);
                break;

            case "B":
                tv.setText("B");
                tv.setBackgroundColor(0xffede0c8);
                tv.setTextColor(0xff000000);
                break;
            case "C":
                tv.setText("C");
                tv.setBackgroundColor(0xfff2b179);
                tv.setTextColor(0xff000000);

                break;
            case "D":
                tv.setText("D");
                tv.setBackgroundColor(0xfff59563);
                tv.setTextColor(0xffffffff);

                break;
            case "E":
                tv.setText("E");
                tv.setBackgroundColor(0xfff67c5f);
                tv.setTextColor(0xffffffff);
                break;
            case "F":
                tv.setText("F");
                tv.setBackgroundColor(0xfff65e3b);
                tv.setTextColor(0xffffffff);

                break;
            case "G":
                tv.setText("G");
                tv.setBackgroundColor(0xfff65e3b);
                tv.setTextColor(0xffffffff);

                break;
            case "H":
                tv.setText("H");
                tv.setBackgroundColor(0xfff65e3b);
                tv.setTextColor(0xffffffff);

                break;
            case "I":
                tv.setText("I");
                tv.setBackgroundColor(0xfff65e3b);
                tv.setTextColor(0xffffffff);

                break;
            default:
                tv.setText("");
                tv.setBackgroundColor(0xffcdc1b4);
                break;


        }

        //   tv.setTextColor(0xffffffff);

    }
}